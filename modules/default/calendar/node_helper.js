/* Magic Mirror
 * Node Helper: Calendar
 *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 */

var NodeHelper = require("node_helper");
var validUrl = require("valid-url");
var CalendarFetcher = require("./calendarfetcher.js");

module.exports = NodeHelper.create({
	// Override start method.
	start: function() {
		var self = this;
		var events = [];

		this.fetchers = [];

		console.log("Starting node helper for: " + this.name);

	},

	// Override socketNotificationReceived method.
	socketNotificationReceived: function(notification, payload) {
		//console.log(payload);
		if (notification === "ADD_CALENDAR") {
			//console.log('ADD_CALENDAR: ');
			this.createFetcher(payload.calConf, payload.fetchInterval, payload.maximumEntries, payload.maximumNumberOfDays);
		}
	},

	/* createFetcher(url, reloadInterval)
	 * Creates a fetcher for a new url if it doesn't exist yet.
	 * Otherwise it reuses the existing one.
	 *
	 * attribute url string - URL of the news feed.
	 * attribute reloadInterval number - Reload interval in milliseconds.
	 */

	createFetcher: function(calConf, fetchInterval, maximumEntries, maximumNumberOfDays) {
		var self = this;
		var label = calConf.label;

		var fetcher;
		if (typeof self.fetchers[label] === "undefined") {
			console.log("Create new calendar fetcher for label: " + label + " - Interval: " + fetchInterval);
			fetcher = new CalendarFetcher(calConf, fetchInterval, maximumEntries, maximumNumberOfDays);

			fetcher.onReceive(function(fetcher) {
				//console.log('Broadcast events.');
				//console.log(fetcher.events());

				self.sendSocketNotification("CALENDAR_EVENTS", {
					label: fetcher.label(),
					events: fetcher.events()
				});
			});

			fetcher.onError(function(fetcher, error) {
				self.sendSocketNotification("FETCH_ERROR", {
					label: fetcher.label(),
					error: error
				});
			});

			self.fetchers[label] = fetcher;
		} else {
			//console.log('Use existing news fetcher for label: ' + label);
			fetcher = self.fetchers[label];
			fetcher.broadcastEvents();
		}

		fetcher.startFetch();
	}
});
