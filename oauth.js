// this script acquires a new oauth token

var fs = require('fs');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var readline = require('readline');

fs.readFile('config/client_secret.json', function process(err,content){
	if(err){
		console.log('Error loading client secret file: ' + err);
		return;
	}
	authorize(JSON.parse(content));
});

function authorize(credentials){
	var clientSecret = credentials.installed.client_secret;
	var clientId = credentials.installed.client_id;
	var redirectUrl = credentials.installed.redirect_uris[0];
	var auth = new googleAuth();
	var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
	var authUrl = oauth2Client.generateAuthUrl({
		access_type: 'offline',
		scope: ['https://www.googleapis.com/auth/calendar.readonly']
	});
	console.log('Authorize by visiting this url: ', authUrl);
	fs.writeFile('authUrl.txt',authUrl);
	console.log('url written to file \'authUrl.txt\' for convenience');
	var rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});
	rl.question('Enter the code from that page here: ', function(code) {
		rl.close();
		oauth2Client.getToken(code, function(err, token) {
			if(err){
				console.log('Error while trying to retrieve access token', err);
				return;
			}
			oauth2Client.credentials = token;
			fs.writeFile('config/calendar-nodejs-mm2.json', JSON.stringify(token));
			console.log('Token saved');
		});
	});
}
